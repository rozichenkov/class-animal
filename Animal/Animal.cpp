

#include <iostream>

using namespace std;

class Animal
{
public:


	virtual void voice() = 0;
	
	
};

class dog :  public Animal
{
public:
	void voice() 
	{
		cout << "Woof" << endl;
	}
};
class cat : public Animal
{
public:
	void voice()
	{
		cout << "Meow" << endl;
	}
};

class cow : public Animal
{
public:
	void voice()
	{
		cout << "Muuuu" << endl;
	}
};

int main()
{
	
	Animal* massiv[3] = {new cat, new dog, new cow};
	for (size_t i = 0; i < 3; ++i)
	{
		massiv[i]->voice();
	}
	for (size_t i = 0; i < 3; ++i) 
	{
		delete massiv[i];
	}
	return 0;
}
